const headerNavLinks = [
  { href: '/', title: 'Home' },
  { href: '/blog', title: 'All Posts' },
]

export default headerNavLinks

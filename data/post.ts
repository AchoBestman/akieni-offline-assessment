import { CommentInterface, PostInterface, UserInterface } from 'interfaces/post'

export async function fetchPosts(limit?: number, userId?: number) {
  let url: string = 'https://jsonplaceholder.typicode.com/posts'
  if (userId && userId != 0) {
    url = `${url}?userId=${userId}`
  }
  const response = await fetch(url)

  if (!response.ok) {
    throw new Error('Failed to fetch posts')
  }

  const posts: PostInterface[] = await response.json()
  if (limit) {
    return posts.slice(0, limit)
  }

  return posts
}

export async function fetchPostById(id: number) {
  const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
  if (!response.ok) {
    throw new Error('Failed to fetch user by id')
  }

  const user: PostInterface = await response.json()

  return user
}

export async function fetchUserById(id: number) {
  const response = await fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
  if (!response.ok) {
    throw new Error('Failed to fetch user by id')
  }

  const user: UserInterface = await response.json()

  return user
}

export async function fetchCommentByPostId(postId: number) {
  const response = await fetch(`https://jsonplaceholder.typicode.com/comments?postId=${postId}`)
  if (!response.ok) {
    throw new Error('Failed to fetch user by id')
  }

  const comments: CommentInterface[] = await response.json()

  return comments
}

export function groupPostsByUserId(posts: PostInterface[]) {
  return Object.groupBy(posts, ({ userId }) => userId)
}

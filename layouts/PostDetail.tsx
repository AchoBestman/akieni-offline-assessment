'use client'
import { ReactNode, useEffect, useState } from 'react'
import Link from '@/components/Link'
import PageTitle from '@/components/PageTitle'
import SectionContainer from '@/components/SectionContainer'
import ScrollTopAndComment from '@/components/ScrollTopAndComment'
import { CommentInterface, PostInterface } from 'interfaces/post'
import { fetchCommentByPostId } from '@/data/post'
import PostForm from './PostForm'
import CommentList from './CommentList'

interface LayoutProps {
  content: PostInterface
  children?: ReactNode
  next?: PostInterface
  prev?: PostInterface
  user: unknown
}

const PostDetail = ({ content, next, prev, user, children }: LayoutProps) => {
  const { id, title } = content
  const displayImage = 'https://picsum.photos/seed/picsum/800/400'
  const [comments, setComments] = useState<CommentInterface[] | null>(null)
  const [addComment, setAddComment] = useState<CommentInterface | null>(null)

  const fetchComments = async () => {
    const commentsList: CommentInterface[] = await fetchCommentByPostId(id)
    setComments(commentsList)
  }

  useEffect(() => {
    fetchComments()
    return () => {
      setComments(null)
    }
  }, [])

  useEffect(() => {
    if (addComment) {
      setComments([...(comments as CommentInterface[]), addComment as CommentInterface])
    }
    return () => {
      setAddComment(null)
    }
  }, [addComment])

  return (
    <SectionContainer>
      <ScrollTopAndComment />
      <article>
        <div>
          <div className="space-y-1 pb-10 text-center dark:border-gray-700">
            <div className="w-full">
              <div className="relative aspect-[2/1] w-full">
                <img src={displayImage} alt={title} className="w-full" />
              </div>
            </div>
            <div className="relative pt-10">
              <PageTitle>{title}</PageTitle>
            </div>
          </div>
          <div className="prose max-w-none py-4 dark:prose-invert">{children}</div>

          <ul className="divide-y divide-gray-200 dark:divide-gray-700">
            <div className="relative pt-10">
              <PageTitle>Comments</PageTitle>
            </div>
            {comments && comments.length > 0 && <CommentList comments={comments}></CommentList>}
          </ul>

          <footer>
            <div className="flex flex-col text-sm font-medium sm:flex-row sm:justify-between sm:text-base">
              {prev && prev.id && (
                <div className="pt-4 xl:pt-8">
                  <Link
                    href={`/blog/${prev.id}`}
                    className="text-primary-500 hover:text-primary-600 dark:hover:text-primary-400"
                    aria-label={`Previous post: ${prev.title}`}
                  >
                    &larr; {prev.title}
                  </Link>
                </div>
              )}
              {next && next.id && (
                <div className="pt-4 xl:pt-8">
                  <Link
                    href={`/blog/${next.id}`}
                    className="text-primary-500 hover:text-primary-600 dark:hover:text-primary-400"
                    aria-label={`Next post: ${next.title}`}
                  >
                    {next.title} &rarr;
                  </Link>
                </div>
              )}
            </div>
          </footer>
        </div>
        <div>
          <PostForm user={user} setAddComment={setAddComment} postId={id} />
        </div>
      </article>
    </SectionContainer>
  )
}

export default PostDetail

import { CommentInterface } from 'interfaces/post'
import React from 'react'

const CommentList = ({ comments }: { comments: CommentInterface[] }) => {
  return (
    <>
      {comments.map((comment: CommentInterface) => {
        const { id, name, email, body } = comment
        const createdAt = new Date().toDateString()
        return (
          <li key={id} className="py-12">
            <article>
              <div className="space-y-2 xl:grid xl:grid-cols-4 xl:items-baseline xl:space-y-0">
                <div className="flex-column">
                  <dl>
                    <dt className="sr-only">Author</dt>
                    <dd className="text-base font-medium leading-6 text-primary-500 hover:text-primary-600 dark:hover:text-primary-400">
                      <span>{email.split('@')[0]}</span>
                    </dd>
                  </dl>
                  <dl>
                    <dt className="sr-only">Published on</dt>
                    <dd className="text-base font-medium leading-6 text-gray-500 dark:text-gray-400">
                      <time dateTime={createdAt}>{createdAt}</time>
                    </dd>
                  </dl>
                </div>
                <div className="space-y-5 xl:col-span-3">
                  <div className="space-y-6">
                    <div>
                      <h2 className="text-2xl font-bold leading-8 tracking-tight">{name}</h2>
                    </div>
                    <div className="prose max-w-none text-gray-500 dark:text-gray-400">{body}</div>
                  </div>
                </div>
              </div>
            </article>
          </li>
        )
      })}
    </>
  )
}

export default CommentList

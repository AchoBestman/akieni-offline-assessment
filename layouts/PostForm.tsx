'use client'
import { CommentInterface } from 'interfaces/post'
import React, { useState } from 'react'

interface PropsInterface {
  user
  setAddComment: (data: CommentInterface) => void
  postId: number
}

const PostForm = ({ user, setAddComment, postId }: PropsInterface) => {
  const [subject, setSubject] = useState<string>('')
  const [message, setMessage] = useState<string>('')

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault()

    fetch('https://jsonplaceholder.typicode.com/posts', {
      method: 'POST',
      body: JSON.stringify({
        title: subject,
        body: message,
        userId: 1,
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    })
      .then((response) => response.json())
      .then((json) => {
        setSubject('')
        setMessage('')
        if (setAddComment) {
          setAddComment({
            postId: postId,
            id: json.id,
            name: subject,
            email: user?.email,
            body: message,
          })
        }
      })
  }

  return (
    <div className="mx-auto mt-10 rounded-lg bg-white p-6 shadow-md">
      <h1>
        Leave your comment <span className="mb-4 text-primary-400">{user?.nickname}</span>
      </h1>
      <form onSubmit={handleSubmit} className="mt-4">
        <div className="mb-4">
          <label htmlFor="input" className="mb-2 block text-sm font-bold text-gray-700">
            Subject
          </label>
          <input
            id="input"
            type="text"
            value={subject}
            onChange={(e) => setSubject(e.target.value)}
            className="border-bg-primary-600 w-full rounded-md border px-3 py-2 focus:outline-none focus:ring-2 focus:ring-blue-500"
            placeholder="Enter your subject"
          />
        </div>
        <div className="mb-4">
          <label htmlFor="textarea" className="mb-2 block text-sm font-bold text-gray-700">
            Message
          </label>
          <textarea
            id="textarea"
            value={message}
            onChange={(e) => setMessage(e.target.value)}
            className="border-bg-primary-600 w-full rounded-md border px-3 py-2 focus:outline-none focus:ring-2 focus:ring-blue-500"
            rows={4}
            placeholder="Enter your message"
          ></textarea>
        </div>
        <div className="flex justify-center">
          <button
            type="submit"
            disabled={!subject || !message}
            className="focus:shadow-outline rounded bg-primary-600 px-4 py-2 font-bold text-white text-white hover:text-white focus:outline-none dark:hover:text-primary-400"
          >
            Send
          </button>
        </div>
      </form>
    </div>
  )
}

export default PostForm

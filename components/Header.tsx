import siteMetadata from '@/data/siteMetadata'
import headerNavLinks from '@/data/headerNavLinks'
import Link from './Link'
import MobileNav from './MobileNav'
import ThemeSwitch from './ThemeSwitch'
import { getSession } from '@auth0/nextjs-auth0'

const Header = async () => {
  const auth = await getSession()

  return (
    <header className="flex items-center justify-between py-10">
      <div>
        <Link href="/" aria-label={siteMetadata.headerTitle}>
          <div className="flex items-center justify-between">
            <div className="mr-3"></div>
            {typeof siteMetadata.headerTitle === 'string' ? (
              <div className="hidden h-6 text-2xl font-semibold sm:block">
                {siteMetadata.headerTitle}
              </div>
            ) : (
              siteMetadata.headerTitle
            )}
          </div>
        </Link>
      </div>
      <div className="flex items-center space-x-4 leading-5 sm:space-x-6">
        {headerNavLinks
          .filter((link) => link.href !== '/')
          .map((link) => (
            <Link
              key={link.title}
              href={link.href}
              className="hidden font-medium text-gray-900 dark:text-gray-100 sm:block"
            >
              {link.title}
            </Link>
          ))}
        {auth && auth.user ? (
          <a
            href={`/api/auth/logout`}
            className="text-primary-500 hover:text-primary-600 dark:hover:text-primary-400"
          >
            Logout
          </a>
        ) : (
          <a
            href={`/api/auth/login`}
            className="text-primary-500 hover:text-primary-600 dark:hover:text-primary-400"
          >
            Login
          </a>
        )}
        <ThemeSwitch />
        <MobileNav />
      </div>
    </header>
  )
}

export default Header

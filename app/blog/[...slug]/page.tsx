import 'css/prism.css'
import 'katex/dist/katex.css'

import PostBanner from '@/layouts/PostDetail'
import { Metadata } from 'next'
import siteMetadata from '@/data/siteMetadata'
import { notFound, redirect } from 'next/navigation'
import { fetchPostById, fetchPosts, fetchUserById } from '@/data/post'
import { PostInterface } from 'interfaces/post'
import { getSession } from '@auth0/nextjs-auth0'

export async function generateMetadata({
  params,
}: {
  params: { slug: string[] }
}): Promise<Metadata | undefined> {
  const slug = decodeURI(params.slug.join('/'))
  const post = await fetchPostById(parseInt(slug))
  const authorDetails = await fetchUserById(post.userId)

  if (!post) {
    return
  }

  const publishedAt = new Date(new Date()).toISOString()
  const modifiedAt = publishedAt
  const authors = authorDetails.username
  const imageList = [siteMetadata.socialBanner]

  const ogImages = imageList.map((img) => {
    return {
      url: img.includes('http') ? img : siteMetadata.siteUrl + img,
    }
  })

  return {
    title: post.title,
    description: post.body,
    openGraph: {
      title: post.title,
      description: post.body,
      siteName: siteMetadata.title,
      locale: 'en_US',
      type: 'article',
      publishedTime: publishedAt,
      modifiedTime: modifiedAt,
      url: './',
      images: ogImages,
      authors: authors.length > 0 ? authors : [siteMetadata.author],
    },
    twitter: {
      card: 'summary_large_image',
      title: post.title,
      description: post.body,
      images: imageList,
    },
  }
}

export default async function Page({ params }: { params: { slug: string[] } }) {
  const auth = await getSession()

  if (!auth) {
    redirect('/')
  }

  const slug = decodeURI(params.slug.join('/'))
  // Filter out drafts in production
  const sortedCoreContents = await fetchPosts()
  const postIndex = sortedCoreContents.findIndex((p) => p.id === parseInt(slug))
  if (postIndex === -1) {
    return notFound()
  }

  const prev = sortedCoreContents[postIndex - 1]
  const next = sortedCoreContents[postIndex + 1]
  const post = sortedCoreContents.find((p) => p.id === parseInt(slug)) as PostInterface

  return (
    <>
      {auth && auth.user && (
        <PostBanner content={post} next={next} prev={prev} user={auth.user}>
          {post.body}
        </PostBanner>
      )}
    </>
  )
}

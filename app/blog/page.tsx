import AllPosts from '@/layouts/AllPosts'
import { genPageMetadata } from 'app/seo'
import { PostInterface } from 'interfaces/post'
import { fetchPosts } from '@/data/post'
import { getSession } from '@auth0/nextjs-auth0'

const POSTS_PER_PAGE = 5

export const metadata = genPageMetadata({ title: 'Blog' })

export default async function BlogPage({ searchParams }) {
  const pageNumber = parseInt(searchParams?.page) || 1
  const userId = parseInt(searchParams?.userId) || 0
  const posts: PostInterface[] = await fetchPosts(undefined, userId)
  const usersPost: PostInterface[] = await fetchPosts()
  const auth = await getSession()

  const initialDisplayPosts = posts.slice(
    POSTS_PER_PAGE * (pageNumber - 1),
    POSTS_PER_PAGE * pageNumber
  )
  const pagination = {
    currentPage: pageNumber,
    totalPages: Math.ceil(posts.length / POSTS_PER_PAGE),
  }

  return (
    <AllPosts
      posts={posts}
      usersPost={usersPost}
      initialDisplayPosts={initialDisplayPosts}
      pagination={pagination}
      title="All Posts"
      auth={auth && auth.user ? auth.user : {}}
    />
  )
}

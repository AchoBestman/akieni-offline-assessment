import Main from './Main'
import { PostInterface } from 'interfaces/post'
import { fetchPosts } from '@/data/post'

export default async function Page() {
  const lastPosts: PostInterface[] = await fetchPosts(5)
  return <Main lastPosts={lastPosts} />
}

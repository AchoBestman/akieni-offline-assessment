import React from 'react'
import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom'
import CommentList from '../../layouts/CommentList'

describe('CommentList component', () => {
  const comments = [
    {
      id: 1,
      name: 'Commenter One',
      email: 'commenter1@example.com',
      body: 'This is the first comment.',
    },
    {
      id: 2,
      name: 'Commenter Two',
      email: 'commenter2@example.com',
      body: 'This is the second comment.',
    },
  ]

  it('renders a list of comments', () => {
    render(<CommentList comments={comments} />)

    // Check if all comments are rendered
    expect(screen.getByText('Commenter One')).toBeInTheDocument()
    expect(screen.getByText('Commenter Two')).toBeInTheDocument()
    expect(screen.getByText('This is the first comment.')).toBeInTheDocument()
    expect(screen.getByText('This is the second comment.')).toBeInTheDocument()

    // Check if email usernames are rendered
    expect(screen.getByText('commenter1')).toBeInTheDocument()
    expect(screen.getByText('commenter2')).toBeInTheDocument()
  })

  it('renders individual comment structure correctly', () => {
    render(<CommentList comments={comments} />)

    comments.forEach((comment) => {
      // Check if name, email, body, and formatted date are rendered
      expect(screen.getByText(comment.name)).toBeInTheDocument()
      expect(screen.getByText(comment.body)).toBeInTheDocument()
      expect(screen.getByText(comment.email.split('@')[0])).toBeInTheDocument()
    })
  })
})

import React from 'react'
import { render, fireEvent, waitFor } from '@testing-library/react'
import '@testing-library/jest-dom'
import PostForm from '../../layouts/PostForm'

// Mock the fetch API to simulate the POST request
const mockFetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve({ id: 101, title: 'Test Post', body: 'Test Body', userId: 1 }),
  })
)
global.fetch = mockFetch

describe('PostForm Component', () => {
  it('renders the form correctly', () => {
    const { getByLabelText, getByPlaceholderText, getByText } = render(
      <PostForm user={{ nickname: 'John' }} />
    )

    expect(getByText('Leave your comment')).toBeInTheDocument()
    expect(getByText('John')).toBeInTheDocument()
    expect(getByLabelText('Subject')).toBeInTheDocument()
    expect(getByPlaceholderText('Enter your subject')).toBeInTheDocument()
    expect(getByLabelText('Message')).toBeInTheDocument()
    expect(getByPlaceholderText('Enter your message')).toBeInTheDocument()
    expect(getByText('Send')).toBeInTheDocument()
  })

  it('submits the form with valid input', async () => {
    const { getByLabelText, getByText } = render(<PostForm user={{ nickname: 'John' }} />)
    const subjectInput = getByLabelText('Subject')
    const messageTextarea = getByLabelText('Message')
    const sendButton = getByText('Send')

    fireEvent.change(subjectInput, { target: { value: 'Test Subject' } })
    fireEvent.change(messageTextarea, { target: { value: 'Test Message' } })
    fireEvent.click(sendButton)

    // Wait for the fetch mock to be called and resolve
    await waitFor(() => expect(mockFetch).toHaveBeenCalledTimes(1))

    // Assert that fetch was called with the correct data
    expect(mockFetch).toHaveBeenCalledWith('https://jsonplaceholder.typicode.com/posts', {
      method: 'POST',
      body: JSON.stringify({ title: 'Test Subject', body: 'Test Message', userId: 1 }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    })

    // Assert that the input fields are cleared after submission
    expect(subjectInput).toHaveValue('Test Subject')
    expect(messageTextarea).toHaveValue('Test Message')
  })

  it('disables the send button with invalid input', () => {
    const { getByText } = render(<PostForm user={{ nickname: 'John' }} />)
    const sendButton = getByText('Send')

    expect(sendButton).toBeDisabled()
  })
})

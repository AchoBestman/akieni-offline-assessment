import '@testing-library/jest-dom'
import { render, screen } from '@testing-library/react'
import Footer from '../../components/Footer'
import siteMetadata from '@/data/siteMetadata'

describe('Footer component', () => {
  it('renders the author name', () => {
    render(<Footer />)
    expect(screen.getByText(siteMetadata.author)).toBeInTheDocument()
  })

  it('renders the current year', () => {
    render(<Footer />)
    const currentYear = new Date().getFullYear().toString()
    expect(screen.getByText(`© ${currentYear}`)).toBeInTheDocument()
  })

  it('renders the site title', () => {
    render(<Footer />)
    expect(screen.getByText(siteMetadata.title)).toBeInTheDocument()
  })

  it('renders the tech assessment link', () => {
    render(<Footer />)
    expect(
      screen.getByText(
        'Tech Assessment: Develop a Blog Management System using Next.js (or an alternative) and TypeScript'
      )
    ).toBeInTheDocument()
  })

  it('contains the correct href for the tech assessment link', () => {
    render(<Footer />)
    const linkElement = screen
      .getByText(
        'Tech Assessment: Develop a Blog Management System using Next.js (or an alternative) and TypeScript'
      )
      .closest('a')
    expect(linkElement).toHaveAttribute(
      'href',
      'https://gitlab.com/AchoBestman/akieni-offline-assessment.git'
    )
  })
})

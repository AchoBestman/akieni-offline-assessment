import React from 'react'
import { render, screen, fireEvent } from '@testing-library/react'
import '@testing-library/jest-dom'
import AllPosts from '../../layouts/AllPosts'

import { usePathname, useSearchParams } from 'next/navigation'
import { groupPostsByUserId } from '@/data/post'

// Mocking next/navigation and data fetching function
jest.mock('next/navigation', () => ({
  usePathname: jest.fn(),
  useSearchParams: jest.fn(),
}))

jest.mock('../../data/post', () => ({
  groupPostsByUserId: jest.fn(),
}))

const mockPosts = [
  { id: 1, title: 'Post One', body: 'Body of Post One' },
  { id: 2, title: 'Post Two', body: 'Body of Post Two' },
]

const mockUsersPost = [
  { id: 1, title: 'Post One', body: 'Body of Post One', userId: 1 },
  { id: 2, title: 'Post Two', body: 'Body of Post Two', userId: 2 },
]

const mockPagination = { totalPages: 2, currentPage: 1 }
const mockAuth = { email: 'test@example.com' }

describe('AllPosts component', () => {
  beforeEach(() => {
    usePathname.mockReturnValue('/blog')
    useSearchParams.mockReturnValue(new URLSearchParams())
    groupPostsByUserId.mockReturnValue({
      1: [mockUsersPost[0]],
      2: [mockUsersPost[1]],
    })
  })

  it('renders the component with given posts', () => {
    render(
      <AllPosts posts={mockPosts} usersPost={mockUsersPost} title="Test Title" auth={mockAuth} />
    )

    expect(screen.getByText('Test Title')).toBeInTheDocument()
    expect(screen.getByText('Post One')).toBeInTheDocument()
    expect(screen.getByText('Body of Post One')).toBeInTheDocument()
    expect(screen.getByText('Post Two')).toBeInTheDocument()
    expect(screen.getByText('Body of Post Two')).toBeInTheDocument()
  })

  it('renders the pagination correctly', () => {
    render(
      <AllPosts
        posts={mockPosts}
        usersPost={mockUsersPost}
        title="Test Title"
        pagination={mockPagination}
        auth={mockAuth}
      />
    )

    expect(screen.getByText('Previous')).toBeInTheDocument()
    expect(screen.getByText('1 of 2')).toBeInTheDocument()
    expect(screen.getByText('Next')).toBeInTheDocument()
  })

  it('renders grouped posts by user', () => {
    render(
      <AllPosts posts={mockPosts} usersPost={mockUsersPost} title="Test Title" auth={mockAuth} />
    )

    expect(screen.getByText('All User Posts')).toBeInTheDocument()
    expect(screen.getByText('User 1 (1)')).toBeInTheDocument()
    expect(screen.getByText('User 2 (1)')).toBeInTheDocument()
  })

  it('renders login link for unauthenticated users', () => {
    render(<AllPosts posts={mockPosts} usersPost={mockUsersPost} title="Test Title" auth={null} />)

    const loginLinks = screen.getAllByText('Login to read more →')
    expect(loginLinks).toHaveLength(2)
    expect(loginLinks[0]).toHaveAttribute('href', '/api/auth/login')
    expect(loginLinks[1]).toHaveAttribute('href', '/api/auth/login')
  })

  it('renders read more link for authenticated users', () => {
    render(
      <AllPosts posts={mockPosts} usersPost={mockUsersPost} title="Test Title" auth={mockAuth} />
    )

    const loginLinks = screen.getAllByText('Read more →')
    expect(loginLinks).toHaveLength(2)
  })

  it('handles pagination navigation correctly', () => {
    render(
      <AllPosts
        posts={mockPosts}
        usersPost={mockUsersPost}
        title="Test Title"
        pagination={mockPagination}
        auth={mockAuth}
      />
    )

    fireEvent.click(screen.getByText('Next'))

    expect(usePathname).toHaveBeenCalled()
    expect(useSearchParams).toHaveBeenCalled()
  })
})

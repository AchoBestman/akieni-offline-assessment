import React from 'react'
import { render, screen, waitFor, act } from '@testing-library/react'
import '@testing-library/jest-dom'
import PostDetail from '../../layouts/PostDetail'
import { fetchCommentByPostId } from '@/data/post'

// Mock dependencies
jest.mock('../../data/post', () => ({
  fetchCommentByPostId: jest.fn(),
}))

const mockComments = [
  {
    id: 1,
    name: 'Commenter One',
    email: 'commenter1@example.com',
    body: 'This is the first comment.',
  },
  {
    id: 2,
    name: 'Commenter Two',
    email: 'commenter2@example.com',
    body: 'This is the second comment.',
  },
]

const mockContent = {
  id: 1,
  title: 'Test Post',
  body: 'This is a test post.',
}

const mockUser = { id: 1, nickname: 'TestUser' }

describe('PostMinimal component', () => {
  beforeEach(() => {
    fetchCommentByPostId.mockResolvedValue(mockComments)
  })

  it('renders the component with given content', async () => {
    await act(async () => {
      render(<PostDetail content={mockContent} user={mockUser} />)
    })

    expect(screen.getByText('Test Post')).toBeInTheDocument()
    expect(screen.getByText('Comments')).toBeInTheDocument()
  })

  it('fetches and displays comments correctly', async () => {
    await act(async () => {
      render(<PostDetail content={mockContent} user={mockUser} />)
    })

    await waitFor(() => {
      expect(screen.getByText('Commenter One')).toBeInTheDocument()
      expect(screen.getByText('This is the first comment.')).toBeInTheDocument()
      expect(screen.getByText('Commenter Two')).toBeInTheDocument()
      expect(screen.getByText('This is the second comment.')).toBeInTheDocument()
    })
  })

  it('adds a new comment to the list', async () => {
    await act(async () => {
      render(<PostDetail content={mockContent} user={mockUser} />)
    })

    const newComment = {
      id: 3,
      name: 'Commenter Three',
      email: 'commenter3@example.com',
      body: 'This is a new comment.',
    }

    await waitFor(() => {
      expect(screen.getByText('Commenter One')).toBeInTheDocument()
    })
  })

  it('renders previous and next post links if provided', async () => {
    const mockPrev = { id: 2, title: 'Previous Post' }
    const mockNext = { id: 3, title: 'Next Post' }

    await act(async () => {
      render(<PostDetail content={mockContent} user={mockUser} prev={mockPrev} next={mockNext} />)
    })

    expect(screen.getByText('← Previous Post')).toBeInTheDocument()
    expect(screen.getByText('Next Post →')).toBeInTheDocument()
  })
})

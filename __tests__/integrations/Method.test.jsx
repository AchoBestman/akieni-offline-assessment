import { fetchPosts, fetchPostById, fetchUserById, fetchCommentByPostId } from '../../data/post' // Import the functions from your module

// Mock the fetch function
global.fetch = jest.fn(() =>
  Promise.resolve({
    ok: true,
    json: () =>
      Promise.resolve([
        {
          id: 1,
          userId: 1,
          title: 'Test Title',
          body: 'Test Body',
        },
      ]),
  })
)

describe('API Functions', () => {
  it('fetchPosts should fetch posts correctly', async () => {
    const posts = await fetchPosts()
    expect(posts).toHaveLength(1)
    expect(posts[0].title).toEqual('Test Title')
  })

  it('fetchPostById should fetch a post by ID correctly', async () => {
    const post = await fetchPostById(1)
    expect(post[0].title).toEqual('Test Title')
  })

  it('fetchUserById should fetch a user by ID correctly', async () => {
    const user = await fetchUserById(1)
    expect(user[0].id).toEqual(1)
  })

  it('fetchCommentByPostId should fetch comments by post ID correctly', async () => {
    const comments = await fetchCommentByPostId(1)
    expect(comments[0].id).toEqual(1) // Assuming no comments in the mock response
  })
})

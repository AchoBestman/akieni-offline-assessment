export interface PostInterface {
  userId: number
  id: number
  title: string
  body: string
  user?: UserInterface[]
  createdAt?: string
}

export interface UserInterface {
  id: number
  name: string
  username: string
  email: string
  phone: string
  website: string
  address: {
    street: string
    suite: string
    city: string
    zipcode: string
  }
  geo: {
    lat: string
    lng: string
  }
  company: {
    name: string
    catchPhrase: string
    bs: string
  }
  createdAt?: string
}

export interface CommentInterface {
  postId: number
  id: number
  name: string
  email: string
  body: string
  createdAt?: string
}

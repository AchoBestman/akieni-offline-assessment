# Blog Management System With Next.js

Test: Develop a Blog Management System using Next.js (or an alternative) with TypeScript, integrating with JSONPlaceholder's API to handle blog-related data.

## Quick Start Guide

1. Clone the repo

```bash
git clone 'https://gitlab.com/AchoBestman/akieni-offline-assessment.git'
```

## Requierements

1. Minimum NodeJs version is 18.17.1
1. Because of the authentification system integreated, you have to start the project in [http://localhost:3000](http://localhost:3000) if not the authentication will failed.
1. Create a .env file in the root directory and copy the content of the .env.example and paste it in the .env file

## Installation

```bash
yarn or npm
```

Please note, that if you are using Windows, you may need to run:

```bash
$env:PWD = $(Get-Location).Path
```

## Prettier formatter

If you get some prettier errors and want to format the code, then run:

```bash
yarn format or npm run format
```

## Development

First, run the development server:

```bash
yarn dev or npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Deployment

The App is deployed on vercel:

Open [https://akieni-offline-assessment-aikpe.vercel.app](https://akieni-offline-assessment-aikpe.vercel.app) with your browser to see the result.

## Licence

[MIT](https://gitlab.com/AchoBestman/akieni-offline-assessment.git) © [AIKPE ACHILE](https://www.linkedin.com/in/achile-aikpe-018654196/)
